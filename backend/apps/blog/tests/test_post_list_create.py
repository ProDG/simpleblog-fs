from rest_framework import status
from rest_framework.test import APITestCase

from apps.blog.factories import UserFactory, PostFactory
from apps.blog.models import Post
from utils.dump_response import dump  # noqa
from utils.helpers_for_tests import AnyInt, AnyStr, utc_datetime


class PostListCreateAPITest(APITestCase):

    def setUp(self) -> None:
        self.user = UserFactory(
            email='user@test.com',
            first_name="Michael",
            last_name="Jackson",
        )
        self.post = PostFactory(
            title="Post title",
            text="Post text",
            created_by=self.user,
        )

    def test_create(self):
        self.client.force_login(self.user)

        response = self.client.post(
            '/api/blog/posts/',
            data={
                'title': "Hello",
                'text': "World"
            }
        )

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(
            {
                "id": AnyInt(),
                "title": "Hello",
                "text": "World",
                "created_on": AnyStr(),  # "2021-11-22T18:14:38.329511Z",
                "created_by": {
                    "id": AnyInt(),
                    "email": "user@test.com",
                    "first_name": "Michael",
                    "last_name": "Jackson"
                }
            },
            response.json()
        )

    def test_create_non_authenticated(self):
        response = self.client.post(
            '/api/blog/posts/',
            data={
                'title': "Hello",
                'text': "World"
            }
        )

        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertEqual(
            {
                "detail": "Authentication credentials were not provided."
            },
            response.json(),
        )

    def test_list_empty(self):
        Post.objects.all().delete()
        response = self.client.get('/api/blog/posts/')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(
            {
                "count": 0,
                "next": None,
                "previous": None,
                "results": []
            },
            response.json()
        )

    def test_list(self):
        response = self.client.get('/api/blog/posts/')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(
            {
                "count": 1,
                "next": None,
                "previous": None,
                "results": [
                    {
                        "id": AnyInt(),
                        "title": "Post title",
                        "text": "Post text",
                        "created_on": AnyStr(),  # "2021-11-22T18:24:43Z",
                        "created_by": {
                            "id": AnyInt(),
                            "email": "user@test.com",
                            "first_name": "Michael",
                            "last_name": "Jackson"
                        }
                    }
                ]
            },
            response.json()
        )

    def test_ordering(self):
        old_post = PostFactory(created_on=utc_datetime(1980, 1, 1))

        response = self.client.get('/api/blog/posts/')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(self.post.id, response.json()['results'][0]['id'])
        self.assertEqual(old_post.id, response.json()['results'][1]['id'])
