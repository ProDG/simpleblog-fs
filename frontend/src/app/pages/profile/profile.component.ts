import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  isPrimary = true;

  constructor() { }

  ngOnInit(): void {
  }

  switchPrimary() {
    this.isPrimary = !this.isPrimary;
  }

}
